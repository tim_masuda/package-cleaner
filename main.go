package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
)

func main() {
	var extensionToDelete string

	fmt.Print("введите расширение файла который хотите удалить к примеру sum  ---    ")

	fmt.Scan(&extensionToDelete)

	if extensionToDelete == "" {
		fmt.Println("расширение файла не может быть пустым")
		return
	}

	filePath := "package.json"

	content, err := ioutil.ReadFile(filePath)

	if err != nil {

		fmt.Printf("ошибка при чтении файла %s: %v\n", filePath, err)
		return
	}

	var packageJSON map[string]interface{}

	err = json.Unmarshal(content, &packageJSON)

	if err != nil {
		fmt.Printf("ошибка при разборе JSON в файле %s %v\n", filePath, err)
		return
	}

	scriptKey := "clean"

	scriptValue := fmt.Sprintf("rimraf *.%s", extensionToDelete)
	if packageJSON["scripts"] == nil {
		packageJSON["scripts"] = make(map[string]interface{})
	}
	packageJSON["scripts"].(map[string]interface{})[scriptKey] = scriptValue

	updatedContent, err := json.MarshalIndent(packageJSON, "", "  ")

	if err != nil {
		fmt.Printf("ошибк при обновлении файла %s %v\n", filePath, err)
		return
	}

	err = ioutil.WriteFile(filePath, updatedContent, 0644)

	if err != nil {
		fmt.Printf("ошибка при записи файла %s: %v\n", filePath, err)
		return
	}

	fmt.Printf("Скрипт \"%s\" добавлен в раздел \"scripts\" в файле %s\n", scriptKey, filePath)

	cmd := exec.Command("npm", "run", "clean")

	cmd.Stdout = os.Stdout

	cmd.Stderr = os.Stderr

	if err := cmd.Run(); err != nil {
		fmt.Printf("Ошибка при выполнении команды npm run clean %v\n", err)
		return
	}

	fmt.Println("файлы успешно удалены согласно скрипту npm run clean")
}
